package dev.reassigningparts.tetris;

public class SimpleTimer {
    private long now;
    private long lastTime;
    private long timer;
    private long duration;

    public SimpleTimer(long duration) {
        this.duration = duration;
        lastTime = System.nanoTime();
        timer = 0;
    }

    public boolean checkTime() {
        now = System.nanoTime();
        timer += now - lastTime;
        lastTime = now;

        if (timer >= duration) {
            timer = 0;
            return true;
        }
        return false;
    }
}
