package dev.reassigningparts.tetris;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class KeyManager implements KeyListener {

    private Set<Integer> keys2;
    private Set<Integer> blockade2;

    public KeyManager() {
        keys2 = new HashSet<>();
        blockade2 = new HashSet<>();
    }

    public boolean isKeyPressed(int key) {
        return keys2.contains(key);
    }

    public boolean getKey(int key) {
        if (keys2.contains(key) && !blockade2.contains(key)) {
            blockade2.add(key);
            return true;
        }
        return false;
    }

    @Override
    public void keyPressed(KeyEvent arg0) {
        keys2.add(arg0.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        keys2.remove(arg0.getKeyCode());
        blockade2.remove(arg0.getKeyCode());
    }

    @Override
    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }

}
