package dev.reassigningparts.tetris.states;

import Utils.Events.EventType;
import Utils.Events.InputType;
import Utils.States.GameStatesManager;
import Utils.States.State;
import dev.reassigningparts.tetris.Handler;
import dev.reassigningparts.tetris.gfx.Settings;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Collections;

public class MainMenuState implements State {

    private final int spaces;
    private final int optionHeight;
    private GameStatesManager owner;

    private enum MenuOptions {
        NEW_GAME, OPTIONS, EXIT;

        private static final java.util.List<MenuOptions> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private final static MenuOptions FIRST_ONE = VALUES.get(0);
        private final static MenuOptions LAST_ONE = VALUES.get(VALUES.size() - 1);

        private MenuOptions nextOption() {
            int nextOption = ordinal() + 1;
            if (nextOption >= MenuOptions.values().length) {
                return FIRST_ONE;
            } else {
                return MenuOptions.values()[nextOption];
            }

        }

        private MenuOptions previousOption() {
            int previousOption = ordinal() - 1;
            if (previousOption < 0) {
                return LAST_ONE;
            } else {
                return MenuOptions.values()[previousOption];
            }
        }
    }

    public MainMenuState() {
        spaces = Settings.getWindowWidth() / 20;
        optionHeight = Settings.getWindowHeight() / 20;
    }

    private MenuOptions selected = MenuOptions.NEW_GAME;


    @Override
    public void onEnter() {
    }

    @Override
    public void onExit() {

    }

    @Override
    public void onUpdate(float deltaTime) {
        if (Handler.getKeyManager().getKey(KeyEvent.VK_UP)) {
            previousOption();
        }
        if (Handler.getKeyManager().getKey(KeyEvent.VK_DOWN)) {
            nextOption();
        }
        if (Handler.getKeyManager().getKey(KeyEvent.VK_ENTER)) {
            if (selected == MenuOptions.NEW_GAME) {
                owner.setState("gameState");
            }
            if (selected == MenuOptions.EXIT) {
                System.exit(0);
            }
        }
    }

    @Override
    public void onDraw(Graphics graphics) {
        graphics.setColor(Color.gray);
        graphics.fillRect(0, 0, Settings.getWindowWidth(), Settings.getWindowHeight());
        for (MenuOptions option : MenuOptions.values()) {
            drawButton(option, graphics);
        }
    }

    @Override
    public void onInput(InputType inputType) {

    }

    @Override
    public void onEvent(EventType eventType) {

    }

    private void drawButton(MenuOptions option, Graphics g) {
        g.setColor(Color.black);
        if (option.equals(selected)) {
            g.fillRect(spaces, (spaces + optionHeight) * option.ordinal() + spaces, Settings.getWindowWidth() - spaces * 2, optionHeight);
            g.setColor(Color.WHITE);
        } else {
            g.drawRect(spaces, (spaces + optionHeight) * option.ordinal() + spaces, Settings.getWindowWidth() - spaces * 2, optionHeight);
        }
        g.drawString(option.name(), Settings.getWindowWidth() / 2 - option.name().length() / 2 * spaces, (optionHeight + spaces) * option.ordinal() + optionHeight / 2 + spaces);
    }

    private void nextOption() {
        selected = selected.nextOption();
    }

    private void previousOption() {
        selected = selected.previousOption();
    }

    @Override
    public void setOwner(GameStatesManager owner) {
        this.owner = owner;
    }
}
