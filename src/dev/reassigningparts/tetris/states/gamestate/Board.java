package dev.reassigningparts.tetris.states.gamestate;

import dev.reassigningparts.tetris.blocks.Block;
import dev.reassigningparts.tetris.blocks.PointTetris;
import dev.reassigningparts.tetris.blocks.Tetromino;
import dev.reassigningparts.tetris.blocks.TetrominoFactory;
import dev.reassigningparts.tetris.gfx.Assets;
import dev.reassigningparts.tetris.gfx.Settings;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Board {

    private static final int DOWN = 1;
    private static final int LEFT = -1;
    private static final int RIGHT = 1;
    private static final int NO_MOVE = 0;

    private int width;
    private int height;
    private Tetromino tetromino;
    private Tetromino shadow;
    private ArrayList<Block> blocksOnMap = new ArrayList<>();
    private PointTetris possitionOfBoard = new PointTetris(20, 20);


    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        generateNewTetromino();
        generateShadowTetromino();
    }

    public boolean checkEndGame() {
        return blocksOnMap.stream().filter(block -> block.getY() <= 2).count() > 0;
    }

    public boolean moveDown() {
        if (canMove(tetromino, NO_MOVE, DOWN)) {
            tetromino.moveDown();
            return true;
        }
        return false;
    }

    public void generateNewTetromino() {
        this.tetromino = TetrominoFactory.randomTetromino();
    }

    public void setNewTetromino(Tetromino tetromino) {
        this.tetromino = tetromino;
        generateShadowTetromino();
    }

    public void generateShadowTetromino() {
        shadow = TetrominoFactory.cloneTetromino(tetromino);
        shadow.changeColor(Assets.temporary);
        while (canMove(shadow, 0, 1))
            shadow.moveDown();
    }

    public void onDraw(Graphics g) {
        g.setColor(Color.black);
        g.drawRect(possitionOfBoard.getX() - 1, -20, Settings.getBlockSize() * width + 1, Settings.getBlockSize() * (height - 2) + 20);
        tetromino.render(g, possitionOfBoard.getX(), 0);
        if (shadow != null)
            shadow.render(g, possitionOfBoard.getX(), 0);
        blocksOnMap.forEach(block -> block.renderWithOffset(g, possitionOfBoard.getX(), 0));
    }

    public List<Block> checkRows() {
        blocksOnMap.addAll(tetromino.getBlocks());
        List<Block> blocksToBeRemoved = new ArrayList<>();
        for (int rowNumber = 0; rowNumber < height; ++rowNumber) {
            blocksToBeRemoved.addAll(checkRow(rowNumber));
        }
        return blocksToBeRemoved;
    }

    public void moveLeft() {
        if (canMove(tetromino, LEFT, NO_MOVE)) {
            tetromino.moveLeft();
            generateShadowTetromino();
        }
    }

    public void moveRight() {
        if (canMove(tetromino, RIGHT, NO_MOVE)) {
            tetromino.moveRight();
            generateShadowTetromino();
        }
    }

    private List<Block> checkRow(final int rowNumber) {
        List<Block> blocksTemp = blocksOnMap.stream().filter(block -> block.getY() == rowNumber).collect(Collectors.toList());
        if (blocksTemp.size() == width) {
            blocksOnMap.removeAll(blocksTemp);
            blocksOnMap.stream().filter(block -> block.getY() < rowNumber).collect(Collectors.toList()).forEach(Block::moveDown);
            generateShadowTetromino();
            return blocksTemp;
        }
        return Collections.emptyList();
    }

    private void newGame() {
        blocksOnMap.clear();
        tetromino = TetrominoFactory.randomTetromino();
    }

    public void rotate() {
        if (canRotate()) {
            tetromino.rotate();
            generateShadowTetromino();
        }
    }

    private boolean canRotate() {
        return Collections.disjoint(getCollisions(), tetromino.getRotationCollisions())
                && staysInBounds(tetromino.getRotationCollisions());
    }

    private boolean canMove(Tetromino tetromino, int x, int y) {
        List<PointTetris> collisionsAfterMove = tetromino.getCollisionsAfterMovingXY(x, y);
        return Collections.disjoint(getCollisions(), collisionsAfterMove) && staysInBounds(collisionsAfterMove);
    }

    private boolean staysInBounds(List<PointTetris> collisions) {
        return collisions.stream().filter(collision -> collision.getX() >= Settings.getBoardWidth()
                || collision.getX() < 0
                || collision.getY() >= Settings.getBoardHeight()).count() == 0;
    }

    private List<PointTetris> getCollisions() {
        return blocksOnMap.stream().map(Block::getPoint).collect(Collectors.toList());
    }
}
