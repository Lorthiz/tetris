package dev.reassigningparts.tetris.states.gamestate;

import dev.reassigningparts.tetris.blocks.Block;
import dev.reassigningparts.tetris.gfx.Assets;
import dev.reassigningparts.tetris.gfx.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Score {

    private static final int posX = 400;
    private static final int posY = 100;

    private int actualValue;
    private int renderedValue;

    private ArrayList<BufferedImage> score;


    public Score() {
        this.actualValue = 0;
        this.renderedValue = 0;
        score = new ArrayList<>(Collections.singletonList(Assets.numbers.get(48)));
    }

    public void increaseScore(List<Block> blocks) {
        switch (blocks.size() / Settings.getBoardWidth()) {
            case 0:
                return;
            case 1:
                actualValue += 10;
                break;
            case 2:
                actualValue += 20;
                break;
            case 3:
                actualValue += 40;
                break;
            case 4:
                actualValue += 80;
                break;
        }
        redrawScore();
    }

    public void render(Graphics graphics) {
        for (int i = 0; i < score.size(); ++i) {
            graphics.drawImage(score.get(i), posX + i * 32, posY, 32, 64, null);
        }
    }

    private void redrawScore() {
        if (actualValue != renderedValue) {
            score.clear();
            renderedValue = actualValue;
            for (char c : String.valueOf(actualValue).toCharArray()) {
                score.add(Assets.numbers.get(Integer.valueOf(c)));
            }
        }
    }
}
