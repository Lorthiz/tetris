package dev.reassigningparts.tetris.states.gamestate;

import Utils.Events.EventType;
import Utils.Events.InputType;
import Utils.States.GameStatesManager;
import Utils.States.State;
import dev.reassigningparts.tetris.Handler;
import dev.reassigningparts.tetris.SimpleTimer;
import dev.reassigningparts.tetris.blocks.Tetromino;
import dev.reassigningparts.tetris.blocks.TetrominoFactory;
import dev.reassigningparts.tetris.gfx.Settings;

import java.awt.*;
import java.awt.event.KeyEvent;

public class GameState implements State {

    private Board gameBoard;
    private GameStatesManager owner;
    private SimpleTimer timerLeftRight, timerDown;
    private static int DOWN_TIMER = 500000000 / 2;
    private static int SIDES_TIMER = 80000000;
    private Score score;
    private Tetromino newOne;


    @Override
    public void onEnter() {
        gameBoard = new Board(Settings.getBoardWidth(), Settings.getBoardHeight());
        timerLeftRight = new SimpleTimer(SIDES_TIMER);
        timerDown = new SimpleTimer(DOWN_TIMER);
        score = new Score();
        newOne = TetrominoFactory.randomTetromino();
    }

    @Override
    public void onExit() {

    }

    @Override
    public void onUpdate(float deltaTime) {
        if (Handler.getKeyManager().getKey(KeyEvent.VK_UP)) {
            gameBoard.rotate();
        }
        if (Handler.getKeyManager().getKey(KeyEvent.VK_DOWN)) {
            while (gameBoard.moveDown()) ;
            fullRow();
        }
        if (timerLeftRight.checkTime()) {
            if (Handler.getKeyManager().isKeyPressed(KeyEvent.VK_RIGHT)) {
                gameBoard.moveRight();
            } else if (Handler.getKeyManager().isKeyPressed(KeyEvent.VK_LEFT)) {
                gameBoard.moveLeft();
            }
        }
        if (timerDown.checkTime() && !gameBoard.moveDown()) {
            fullRow();
        }



        if (gameBoard.checkEndGame() || Handler.getKeyManager().getKey(KeyEvent.VK_ESCAPE)) {
            owner.setState("mainMenu");
        }
    }

    private void fullRow() {
        score.increaseScore(gameBoard.checkRows());
        gameBoard.setNewTetromino(newOne);
        newOne = TetrominoFactory.randomTetromino();
    }

    @Override
    public void onDraw(Graphics graphics) {
        gameBoard.onDraw(graphics);
        score.render(graphics);
        newOne.render(graphics, 300, 400);
    }

    @Override
    public void onInput(InputType inputType) {

    }

    @Override
    public void onEvent(EventType eventType) {

    }

    @Override
    public void setOwner(GameStatesManager owner) {
        this.owner = owner;
    }
}
