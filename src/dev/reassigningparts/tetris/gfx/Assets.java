package dev.reassigningparts.tetris.gfx;

import java.awt.image.BufferedImage;
import java.util.HashMap;


public class Assets {

    private static final int width = 64, height = 64;
    private static final int scoreWidth = 32, scoreHeight = 64;

    public static BufferedImage red, green, blue, cyan, purple, yellow, grey, temporary;
    public static HashMap<Integer, BufferedImage> numbers = new HashMap<>();

    static {
        SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("../../../../textures/sheet.png"));
        red = sheet.crop(0, 0, width, height);
        green = sheet.crop(width, 0, width, height);
        blue = sheet.crop(2 * width, 0, width, height);
        cyan = sheet.crop(3 * width, 0, width, height);
        purple = sheet.crop(4 * width, 0, width, height);
        yellow = sheet.crop(5 * width, 0, width, height);
        grey = sheet.crop(6 * width, 0, width, height);
        temporary = sheet.crop(7 * width, 0, width, height);

        sheet = new SpriteSheet(ImageLoader.loadImage("../../../../textures/score.png"));

        for (int i = 48; i < 58; ++i) {
            numbers.put(i, sheet.crop((i - 48) * scoreWidth, 0, scoreWidth, scoreHeight));
        }
    }
}
