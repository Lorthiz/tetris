package dev.reassigningparts.tetris.gfx;

import dev.reassigningparts.tetris.blocks.PointTetris;

public class Settings {

    private static int windowWidth;
    private static int windowHeight;

    private static int blockSize;

    private static int boardWidth;
    private static int boardHeight;

    private static PointTetris spawnPoint;

    static {
        blockSize = 32;
        boardWidth = 10;
        boardHeight = 22;
        spawnPoint = new PointTetris(boardWidth / 2 - 1, 0);
        windowWidth = boardWidth * blockSize + 400;
        windowHeight = (boardHeight - 2) * blockSize + 20;
    }

    private Settings() {
    }

    public static int getBoardHeight() {
        return boardHeight;
    }

    public static int getBoardWidth() {
        return boardWidth;
    }

    public static int getBlockSize() {
        return blockSize;
    }

    public static int getWindowHeight() {
        return windowHeight;
    }

    public static int getWindowWidth() {
        return windowWidth;
    }

    public static PointTetris getSpawnPoint() {
        return new PointTetris(spawnPoint);
    }
}
