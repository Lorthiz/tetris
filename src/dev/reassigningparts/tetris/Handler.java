package dev.reassigningparts.tetris;

public class Handler {

    private static KeyManager keyManager;

    static {
        keyManager = new KeyManager();
    }

    private Handler() {
    }

    public static KeyManager getKeyManager() {
        return keyManager;
    }
}
