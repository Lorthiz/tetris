package dev.reassigningparts.tetris.blocks;

import dev.reassigningparts.tetris.gfx.Assets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TetrominoZ extends Tetromino {

    private static final Map<Tetromino.Status, List<PointTetris>> vectors;

    static {
        vectors = new HashMap<>();
        List<PointTetris> points = new ArrayList<>();
        points.add(new PointTetris(-1, 0));
        points.add(new PointTetris(0, 0));
        points.add(new PointTetris(0, 1));
        points.add(new PointTetris(1, 1));
        vectors.put(Tetromino.Status.FIRST, points);

        points = new ArrayList<>();
        points.add(new PointTetris(0, 1));
        points.add(new PointTetris(0, 0));
        points.add(new PointTetris(1, 0));
        points.add(new PointTetris(1, -1));
        vectors.put(Tetromino.Status.SECOND, points);
    }

    public TetrominoZ() {
        super(TetrominoFactory.Type.Z);
        vectors.get(acutalStatus).stream().forEach(pos -> blocks.add(new Block(position.getX() + pos.getX(), position.getY() + pos.getY(), Assets.yellow)));
        acutalStatus = Tetromino.Status.SECOND;
    }

    @Override
    public void rotate() {
        blocks.clear();
        vectors.get(acutalStatus).stream().forEach(pos -> blocks.add(new Block(position.getX() + pos.getX(), position.getY() + pos.getY(), Assets.yellow)));
        switch (acutalStatus) {
            case FIRST:
                acutalStatus = Tetromino.Status.SECOND;
                break;
            case SECOND:
                acutalStatus = Tetromino.Status.FIRST;
                break;
        }
    }

    @Override
    public List<PointTetris> getRotationCollisions() {
        return vectors.get(acutalStatus).stream().map(block -> new PointTetris(position.getX() + block.getX(), position.getY() + block.getY())).collect(Collectors.toList());
    }

}