package dev.reassigningparts.tetris.blocks;

import dev.reassigningparts.tetris.gfx.Assets;

import java.util.*;

public class TetrominoO extends Tetromino {

    private static final Map<Status, List<PointTetris>> vectors;

    static {
        vectors = new HashMap<>();
        List<PointTetris> points = new ArrayList<>();
        points.add(new PointTetris(-1, 0));
        points.add(new PointTetris(0, 0));
        points.add(new PointTetris(0, 1));
        points.add(new PointTetris(-1, 1));
        vectors.put(Status.FIRST, points);
    }

    public TetrominoO() {
        super(TetrominoFactory.Type.O);
        vectors.get(acutalStatus).stream().forEach(pos -> blocks.add(new Block(position.getX() + pos.getX(), position.getY() + pos.getY(), Assets.cyan)));
    }

    @Override
    public void rotate() {
        //nothing to do in this one
    }

    @Override
    public List<PointTetris> getRotationCollisions() {
        return Collections.emptyList();
    }


}
