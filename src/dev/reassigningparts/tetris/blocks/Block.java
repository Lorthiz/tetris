package dev.reassigningparts.tetris.blocks;


import dev.reassigningparts.tetris.gfx.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;


public class Block {

    private BufferedImage color;
    private PointTetris point;

    public Block(int x, int y, BufferedImage color) {
        this.point = new PointTetris(x, y);
        this.color = color;
    }

    public void moveDown() {
        point.moveY(1);
    }

    public void moveRight() {
        point.moveX(1);
    }

    public void moveLeft() {
        point.moveX(-1);
    }

    public void moveToPos(int x, int y) {
        point.moveToPos(x, y);
    }

    public void move(int x, int y) {
        point.move(x, y);
    }

    public void render(Graphics g) {
        int blockSize = Settings.getBlockSize();
        g.drawImage(color,
                point.getX() * blockSize + 1,
                (point.getY() - 2) * blockSize + 1,
                blockSize - 2,
                blockSize - 2,
                null);
    }

    public void renderWithOffset(Graphics g, int offsetX, int offsetY) {
        int blockSize = Settings.getBlockSize();
        g.drawImage(color,
                point.getX() * blockSize + 1 + offsetX,
                (point.getY() - 2) * blockSize + 1 + offsetY,
                blockSize - 2,
                blockSize - 2,
                null);
    }

    public int getX() {
        return point.getX();
    }

    public int getY() {
        return point.getY();
    }

    public void setColor(BufferedImage color) {
        this.color = color;
    }

    public BufferedImage getColor() {
        return color;
    }

    public PointTetris getPoint() {
        return new PointTetris(point);
    }

}
