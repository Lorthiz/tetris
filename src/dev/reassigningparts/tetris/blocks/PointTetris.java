package dev.reassigningparts.tetris.blocks;

/**
 * Created by Lorthiz on 2016-05-02.
 */
public class PointTetris {

    private int x, y;

    public PointTetris(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public PointTetris(PointTetris point) {
        this.x = point.x;
        this.y = point.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PointTetris point = (PointTetris) o;

        if (x != point.x) return false;
        return y == point.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public void move(int x, int y) {
        this.x += x;
        this.y += y;

    }

    public void moveToPos(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void moveX(int x) {
        this.x += x;
    }

    public void moveY(int y) {
        this.y += y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
