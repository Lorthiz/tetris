package dev.reassigningparts.tetris.blocks;

import dev.reassigningparts.tetris.gfx.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Tetromino {

    protected ArrayList<Block> blocks;
    protected Status acutalStatus;
    protected PointTetris position;
    protected TetrominoFactory.Type type;



    protected enum Status {
        FIRST, SECOND, THIRD, FOURTH;
    }
    protected Tetromino(TetrominoFactory.Type type) {
        blocks = new ArrayList<>();
        acutalStatus = Status.FIRST;
        position = Settings.getSpawnPoint();
        this.type = type;
    }

    public void changeColor(BufferedImage color) {
        blocks.stream().forEach(block -> block.setColor(color));
    }

    public void moveDown() {
        position.move(0, 1);
        blocks.stream().forEach(Block::moveDown);
    }

    public void moveRight() {
        position.move(1, 0);
        blocks.stream().forEach(Block::moveRight);
    }

    public void moveLeft() {
        position.move(-1, 0);
        blocks.stream().forEach(Block::moveLeft);
    }

    public void render(Graphics g) {
        blocks.stream().forEach(block -> block.render(g));
    }

    public void render(Graphics g, int offsetX, int offsetY) {
        blocks.stream().forEach(block -> block.renderWithOffset(g, offsetX, offsetY));
    }


    public List<PointTetris> getCollisionsAfterMovingXY(int x, int y) {
        return blocks.stream().map(block -> {
            PointTetris point = block.getPoint();
            point.move(x, y);
            return point;
        }).collect(Collectors.toList());
    }

    public List<PointTetris> getCollisions() {
        return blocks.stream().map(Block::getPoint).collect(Collectors.toList());
    }

    public abstract void rotate();

    public abstract List<PointTetris> getRotationCollisions();

    public ArrayList<Block> getBlocks() {
        return blocks;
    }

    public TetrominoFactory.Type getType() {
        return type;
    }
}
