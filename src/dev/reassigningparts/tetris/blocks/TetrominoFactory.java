package dev.reassigningparts.tetris.blocks;

import java.awt.image.BufferedImage;
import java.util.*;
import java.util.stream.Collectors;

public class TetrominoFactory {

    public enum Type {
        O {
            @Override
            Tetromino create() {
                return new TetrominoO();
            }
        }, I {
            @Override
            Tetromino create() {
                return new TetrominoI();
            }
        }, S {
            @Override
            Tetromino create() {
                return new TetrominoS();
            }
        }, Z {
            @Override
            Tetromino create() {
                return new TetrominoZ();
            }
        }, L {
            @Override
            Tetromino create() {
                return new TetrominoL();
            }
        }, J {
            @Override
            Tetromino create() {
                return new TetrominoJ();
            }
        }, T {
            @Override
            Tetromino create() {
                return new TetrominoT();
            }
        };

        private static final List<Type> VALUES =
                Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static Tetromino randomTetromino()  {
            return VALUES.get(RANDOM.nextInt(SIZE)).create();
        }

        abstract Tetromino create();
    }

    public static Tetromino cloneTetromino(Tetromino tetromino) {
        Tetromino newOne = newFromType(tetromino.getType());
        newOne.blocks = new ArrayList<>(tetromino.getBlocks().size());
        BufferedImage color = tetromino.blocks.stream().findAny().get().getColor();
        newOne.blocks.addAll(tetromino.getBlocks().stream().map(block -> new Block(block.getX(), block.getY(), color)).collect(Collectors.toList()));
        newOne.acutalStatus = tetromino.acutalStatus;
        newOne.position = new PointTetris(tetromino.position);
        return newOne;
    }

    public static Tetromino newFromType(Type type) {
        return type.create();
    }

    public static Tetromino randomTetromino() {
        return Type.randomTetromino();
    }
}
