package dev.reassigningparts.tetris;

import Utils.ApplicationSpine.Game;
import Utils.Graphics.Display;
import dev.reassigningparts.tetris.gfx.Settings;
import dev.reassigningparts.tetris.states.MainMenuState;
import dev.reassigningparts.tetris.states.gamestate.GameState;

public class TetrisGame extends Game {
    @Override
    public void onStartup() {
        gameStatesManager.addState("mainMenu", new MainMenuState());
        gameStatesManager.addState("gameState", new GameState());
        gameStatesManager.setState("mainMenu");
        display = new Display(Settings.getWindowWidth(), Settings.getWindowHeight(), "Tetris");
        display.getFrame().addKeyListener(Handler.getKeyManager());
    }
}
